# Summary

[Introduction](./introduction.md)

- [Prise de parole en publique](./prise_de_parole_en_public/prise_de_parole_en_public.md)
  - [Autointerviews Orateurs](./prise_de_parole_en_public/autointerviews_orateurs.md)
  - [Podcast qui n'a pas encore de nom](./prise_de_parole_en_public/podcast.md)
  - [D'autres sources]()
- [Conférences données](./conferences_donnees/conferences_donnees.md)
  - [Des Portes Logiques Pneumatiques en Bois](./conferences_donnees/portes_logiques_pneumatiques_en_bois.md)
  - [The OldMan Glitch](./conferences_donnees/the_oldman_glitch.md)
  - [Des trucs et des machins qui aident à faire des choses](./conferences_donnees/des_trucs_et_des_machins.md)
- [Ateliers Animés]()
  - [EvenStorming]()
  - [Scratch à l'école]()
  - [Scratch et EventSroming]()
  - [Ecrire et proposer un sujet dans une conférence]()
  - [Introduction à Rust et à l'Embarqué]()
- [Programmation]()
  - [Macros Rust du point de vue d'un débutant](./programmation/rust_macros_from_a_beginner_point_of_view.md)
  - [Notes: démarrage d'un projet Rust sur STM32F103 (BluePill) [EN]](./programmation/rust_STM32F103.md)
  - [Apprendre WASM depuis ses spécifications [EN]](./programmation/tywafs.md)
  - [Atelier Introduction à Rust et à l'Embarqué](./programmation/atelier_rust_embarque.md)
  - [mdbook avec Gitlab CI](./programmation/mdbook_avec_gitlab_CI.md)
  - [Première pull request](./programmation/premierepullrequest.md)
- [Développement]()
  - [Accompagnement]()
  - [Definition de produit]()
  - [Itérations]()

[Contact](./contact.md)
[Information sur les cookies](./cookies.md)