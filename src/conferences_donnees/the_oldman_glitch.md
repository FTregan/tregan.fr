# The OldMan Glitch

The OldMan Glitch est un glitch[^1] dans la première génération de Pokémon[^2] sur Gameboy. Dans cette présentation, effectuée entièrement sur un émulateur, je propose de montrer la manipulation tout en expliquant dans le détails ses mécanismes : de parler d'off by one error, d'underflow, de buffer overflow, de payload, d'injection de code et d'assembleur, tout ça en s'amusant à faire déconner Pokémon autant que possible.

![](the_oldman_glitch.png)

[Captation au DevFest Toulouse 2018](https://www.youtube.com/watch?v=hBo28RVftNc) (youtube)

J'ai donné cette présentation en espérant faire prendre conscience à des développeurs ou  développeuses que ne pas complètement abandonner la sécurité est à la fois possible, intéressant et utile. Et qu'un bug, ça peut être beau.

## Conférences

- [Meetup Toulouse JUG](https://www.toulousejug.org/), lightning talk 25 minutes
- [DevFest Toulouse 2018](https://2018.devfesttoulouse.fr/), format 40 minutes
- [RivieraDev 2019](https://2019.rivieradev.fr), format 35 minutes
- [Sunny Tech](https://2019.sunny-tech.io/) format 45 minutes
- [THSF 2019](https://www.thsf.net/), format 45 minutes, grand public averti
- [Meetup Lyon Software Crafters Community](https://www.meetup.com/Software-Craftsmanship-Lyon/), format 60 minutes avec discussion, à distance (twitch)

## Fichiers de la présentation

La liste des outils, les sauvegardes, le script détaillé des manipulations et des explications, ainsi que la description du setup - c'est à dire tout ce qu'il faut[^3] pour rejouer la présentation chez soi ou la redonner - sont disponibles [sur github](https://github.com/FabienTregan/TheOldManGlitch).

---

[^1]: Un état non voulu par les développeurs dans lequel on peut mettre de façon temporaire un jeu et qui permet d'obtenir des effets non prévus.

[^2]: Pokémon Rouge et Pokémon Bleu, sortis en 1996 au japon et en 1999 en France.

[^3]: A l'exception de la ROM