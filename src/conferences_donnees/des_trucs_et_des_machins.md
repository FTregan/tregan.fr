# Des trucs et des machins qui aident à faire des choses

Cette présentation devait porter à l'origine sur la similitudes de topologie des problèmes que cherchent à résoudre un métier à tisser et ordinateur. C'est la première présentation que j'ai accepté de donner avant de l'avoir entièrement écrite ou réfléchie, et après quatre mois de rechercher et de travail, le sujet a beaucoup évolué.

J'y parle finalement de l'évolution des abaques de la mésopotamie à la machine de Babbage, en essayant montrer les mécanismes et de parler de certaines problématiques traversées pendant cet histoire qui résonnent toujours avec certaines questions que posent aujourd'hui l'informatique et le développement logiciel.

![](pascaline.jpg)

[Captation au DevFest Toulouse 2019](https://www.youtube.com/watch?v=LHzVkjHjSso) (Si vous prenez de le temps de regarder cette présentation, merci de lire aussi ce [twitt](https://twitter.com/FTregan/status/1180059776371306496?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1180059776371306496%7Ctwgr%5E%7Ctwcon%5Es1_c10&ref_url=https%3A%2F%2F2019.devfesttoulouse.fr%2Fsessions%2Fkeynote_open%2F) de complément.)

Je devais y parler aussi de métiers à tisser, mais l'impossibilité de trier parmi tout ce que j'aurais eu envie de raconter de ces quatre mois ne m'en ont pas laissé le temps.

C'est une présentation qui a été très frustrante (tellement peu de temps pour tant de travail et de choses à dire). J'ai fini par me réconcilier avec elle et j'y reviendrais peut être sous forme d'atelier quand les rencontres physiques redeviendront plus faciles.

## Conférence

- [DevFest Toulouse 2019](https://2018.devfesttoulouse.fr/), Keynote d'ouverture, 30 minutes.

## Fichiers de la présentation

Les fichiers de la présentation n'ont pas été publiés, n'hésitez pas à me les demander s'ils vous intéressent.