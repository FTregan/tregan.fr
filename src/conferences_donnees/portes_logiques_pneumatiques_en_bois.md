# Des Portes Logiques Pneumatiques en Bois

Une présentation issue d'un projet personnel, qui parle d'orgues de barbaries, de transistors, d'électronique, mais aussi de rater des choses, de procrastination, de trouver des choses qui marchent pour soi, de baby step, d'artisanat, d'esthétique de la technique...

![](vanne.jpg)

[Captation au BreizhCamp 2018](https://www.youtube.com/watch?v=JWB2W6tWz5Y) (youtube)

## Conférences

- [DevFest Toulouse 2017](https://2017.devfesttoulouse.fr/), format 20 minutes entre midi et deux
- [BreizhCamp 2018](http://2018.breizhcamp.org/), en keynote (35 minutes)
- [MiXiT 2018](https://mixitconf.org/) en format Alien, public plus orienté UX et Accessibilité
- [THSF 2018](https://www.thsf.net/), format 45 minutes, grand public

## Fichiers de la présentation

Les fichiers sont disponibles [sur github](https://github.com/FabienTregan/PortesLogiquesPneumatiques).