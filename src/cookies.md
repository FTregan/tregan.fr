# Information sur les cookies

Il n'y en a pas.

Le local storage est utilisé pour sauvegarder le thème[^1] choisi. Cette information reste sur votre navigateur et n'est jamais envoyée au serveur.

---

[^1]: Cliquer sur le pinceau en haut à gauche des articles.
