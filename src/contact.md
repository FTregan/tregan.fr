# Contact

- Twitter: [@Twitter](https://twitter.com/ftregan) (DM ouverts mais le système de filtre de twitter n'est pas optimal)
-  Mail: fabien at tregan point fr

Pour suggérer des corrections ou améliorations sur ce site, une possibilité est d'utiliser le système de pull request ou issues de gitlab. Mais si Twitter ou le Mail sont plus simple pour vous, n'hésitez pas.