# NoCV

J'ai été consultant en SSII pendant presque huit ans. J'ai appris l'exercice de la présentation de parcours : adapter le contenu, cadencer le rythme, être lisible, répondre aux questions qui n'ont pas encore été posées, équilibrer le coté technique et le coté humain... rassurer le client. J'avais de *bons résultats*.

J'ai ensuite travaillé comme responsable technique de projet et architecte dans l'aéronautique pendant quatre ans. Je recevais des CV sélectionnés selon des mots clefs, des *rangs* d'écoles, ... Je les lisais sans savoir quoi y chercher et sans y trouver grand chose. A l'entretien technique, que la réponse ait été ou pas dans le CV, ce que j'essayais de savoir n'était jamais quelles quantité de tel langage la personne avait écrit, ni quelle version de telle librairie ou tel framework elle connaissait, mais si la personne savait dire "je ne sais pas", si elle arrivait à comprendre mes explications[^1], si elle était plus intéressée par comment faire les choses ou par pourquoi les faire comme ça... Toutes ces choses qui font que justement, les informations que j'avais avant l'entretient n'étaient pas très importantes. Je n'ai pas vraiment trouvé à l'époque le format d'entretien que j'aurais voulu. Avec le recul, j'aurais pu me contenter de raconter le projet aux personnes et de leur demander ce qu'elles auraient voulu essayer d'y apporter et ce qu'elles espéraient qu'on sache leur apporter... 

J'ai appris plus tard à comprendre ce que j'avais perdu malgré mes efforts dans cette période de ma vie, puis à retrouver petit à petit le plaisir que j'avais eu à programmer quand j'avais dix, quinze ou vingt ans[^2]. Je suis devenu freelance et ai commencé à beaucoup moins "travailler" - si travailler veut dire "facturer son temps" - et à beaucoup plus "faire des trucs". Enfin à essayer.

Je me suis demandé, beaucoup, comment faire part de tout ceci dans un CV ; comment y raconter qui je suis, comment laisser le lecteur décider de ce qu'il pense que je pourrais ou pas apporter à son projet et/ou à son équipe. Et puis j'ai admis que ca ne tiendrait pas sur une page A4 en trois parties avec les mot-clés nécessaires. J'ai essayé d'autres [choses](https://twitter.com/FTregan/status/981492294544248832?s=20) [^3]. J'ai fais ce site un peu pour ceci, beaucoup pour écrire tout un tat de choses que je voudrais ne pas oublier de mon parcours.

Il faudra forcément un peu plus de temps pour le consulter que pour screener un CV : si vous vouliez vraiment regarder les vidéos, lire le code... il faudrait probablement un après-midi entier. Mais ce n'est pas l'idée, je voudrais plutôt vous inviter à trouver et à vous attarder sur ce qui pourrait avoir de la valeur pour vous.

Si je devais synthétiser de quelle façon je voudrais contribuer à une équipe, ce pourrait être ça : "Inviter à prendre le temps de mieux faire ce qui pourrait vraiment avoir de la valeur pour nous."

---
[^1]: J'ai, je crois, beaucoup progressé en explications depuis. Je l'espère en tout cas.

[^2]: J'en ai bientôt quarante-quatre.

[^3]: Ce tweet reste d'actualité, mais je mettrais Rust en têtes des choses que je voudrais pratiquer. (Et Bruxelles serait remplacé par l'est de la Belgique ou Luxembourg.)