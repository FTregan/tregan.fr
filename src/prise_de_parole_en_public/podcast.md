# Podcast qui n'a pas encore de nom

On peut avoir trente ans de programmation derrière soit et ne pas tout connaitre. On peut ne pas oser poser une question, on peut oublier d'en prendre le temps. On peut croire déranger une personne qui n'ose pas prendre la parole d'elle-même où qui ne se sent pas légitime ou encore qui pensent que si elle sait, tout le monde sait déjà.

On peut avoir trouvé des réponses longues, précises, détaillées, ou markettées, mais avoir envie d'autre chose.

J'ai eu envie de tester quelque chose :
- Une question simple dont on n'a pas forcément la réponse
- Posée à quelqu'un qu'on a envie d'entendre sur le sujet
- Une réponse courte et a peu prêt accessible

Sur un podcast qui se veut avant tout collaboratif et communautaire. (voir plus bas pour contribuer)

## Les épisodes enregistrés

- les [Stack Canaries](http://treguy.free.fr/Quelques%20minutes%20pour%20me%20raconter%20ca/stack%20canaries.mp3), par [Emy](https://twitter.com/entropyqueen_)
- les [Lenses](http://treguy.free.fr/Quelques%20minutes%20pour%20me%20raconter%20ca/lense.ogg) par [François](https://twitter.com/fteychene)
- l'[Affichage sur l'atari 2600](http://treguy.free.fr/Quelques%20minutes%20pour%20me%20raconter%20ca/affichage%202600.ogg) par [Fabien](https://twitter.com/ftregan) (c'est moi)

## Les question encore en cours

- la [Machine de Von Neumann](http://treguy.free.fr/Quelques%20minutes%20pour%20me%20raconter%20ca/machine%20de%20Von%20Neumann%20-%20question.ogg) (frederic)
- les [différentes formes de Polymorphisme](http://treguy.free.fr/Quelques%20minutes%20pour%20me%20raconter%20ca/polymorphisme%20-%20question.ogg) (didier)
- les Lambdas (vincent)
- les tests PCR (kromette)
- le systeme d'exploitation
- reverse proxy et load balancer
- process, thread et coroutine

## Contribuer

Je prends les questions, les suggestions de personnes à interroger, les relectures, les avis techniques. En fait il me faudrait même de l'aide pour faire connaitre le projet, lui trouver un nom, faire un site web, le rendre lisible par les lecteurs de podcast, l'indexer, trouver un logo... Il me manque aussi probablement beaucoup de compétences sur le traitement du son et d'autres choses encore que je ne vois même pas. Si quelqu'un veut contribuer, il y a surement de la place pour lui ou elle. 

[Me contacter](../contact.md).

  ### Questions

La bonne question serait avant tout celle que vous n'avez pas posée, pour laquelle vous n'avez toujours pas de réponse simple ou pour laquelle vous avez entendu une réponse dont vous vous êtes dit après coup que finalement, elle était utile.

Les questions portent sur la définition d'un terme technique ou de différences entre deux termes. 

Si tu as d'autres questions à poser, si tu veux poser une question dont tu connais très bien la réponse mais tu penses qu'elle mérite qu'on lui accorde quelques minutes, parlons en.

Dans la question, on essaie de présenter très rapidement la personne à qui on la pose - pas pour justifier de sa pertinence mais pour donner un contexte à l'auditeur. On peut suggérer des éléments de réponse, mais la question doit être ouverte, elle ne sera de toute façon enregistrée qu'après la réponse de façon à pouvoir la modifier pour coller à cette dernière. Il n'y a pas de réponse hors sujet.

  ### Suggérer quelqu'un à qui donner la parole

S'il ya quelqu'un que tu as envie qu'on entendre plus, ou si toi-même tu voudrais t'entrainer à prendre la parole et construire une présentation sur un format court et différé, n'hésite pas à me contacter. On trouvera probablement la bonne question.

  ### Répondre

Si je t'ai soumis une question, c'est que quelqu'un a pensé que tu étais un bonne personne à interroger, et je n'en doute pas.

Les réponses sont très libres. Il n'y a pas de réponses hors sujet, ou pas assez complètes. L'idée est plus de montrer que que telle ou telle personne répond à cette question. Il y aura, j'espère, des questions avec plusieurs réponses. Les questions sont enregistrer après les réponses pour introduire celle-ci au mieux et présenter la personne qui répond d'une façon faite en accord avec elle.

Je peux aider à construire la réponse, faire des retours sur la premières version enregistrée, ou aider à trouver la bonne personne pour accompagner la personne qui répond.

Voici quelques guides pour répondre :
- Les réponses sont diffusées en audio (avec peut être un transcript dans le futur), on peut trouver quelqu'un pour lire si tu préfère  contribuer par écrit.
- Les réponses sont de préférence relativement anonymes (on donnera bien sûr le compte twitter ou github de la personne qui répond si elle le souhaite, mais on n'est pas là pour montrer qui est super fort ou vend quoi sur tel ou tel sujet)
- Il y a beaucoup de façon d'être "dev", tout le monde ne sait pas ce qu'est un registre, un DMA, le BDD ou DDD. Même si on s'autorise les réponses très techniques, essayons de s'assurer que tout "dev" comprenne au moins l'idée.
- La réponse est relativement courte, peut être entre deux et cinq minutes.
- Il peut y avoir une slide (une imagine d'illustration), mais il est probable que tout le monde ne l'ai pas sous les yeux en écoutant la réponse et il faudrait qu'elle ne soit pas nécessaire à la compréhension.

L'idée est d'avoir une question et une réponse que l'on puisse écouter en marchant tranquillement le matin en allant au travail.
