# Autointerviews Orateurs

Un jour au hasard d'une conversation sur Slack, [quelqu'un](https://twitter.com/jeanMartiF) qui ne parlait pas dans des conférences me demanda "Ca t'apporte quoi de parler en public ?"

La question m'interpela, à la fois parce que je ne me l'étais jamais posée - je le faisais c'est tout -, et parce qu'elle me rappela les nombreuses questions que je m'étais posées lors de mes premières conférences, quand je mettais les pieds au milieu de tous ces gens qui se connaissaient, avaient l'air de savoir ce qu'il se passait et de trouver ça normal.

Celà m'a donné envie à la fois de poser cette question (et quelques autres), et de rendre les réponses publiques pour désacraliser un peu le "status" de *speaker*.

Il restait à trouver un format, pour le tester rapidement et pour permettre que chacun.e réponde comme ils ou elles le voulaient. J'ai fais un simple repository github avec un template d'issue - j'avais de bonne chance de toucher en premier des personnes ayant déjà un compte -, j'ai posté sur twitter, ça a parlé au gens, le mot a, j'ai eu plein de réponses dont la plupart m'ont étonnées.

Il y a même eu quelques émules (réponses faites en meetup avant les talks) et utilisation (en ateliers de préparation à la prise de parole) qu'on m'a remontées. Il resterait à trouver à ce projet un endroit pour le rendre plus visible ; pour que les personnes à qui il pourrait servir tombent dessus. En attendant, il est là : <https://github.com/FabienTregan/autointerview-orateurs>