# Prise de parole en publique

Dans le chapitre suivant, je raconte comment j'ai commencé à parler en public vers quarante ans.

Je n'aime pas les *success stories* pleines de promesses - en bonne parties dûes aux hasards et à des avantages pas forcément visibles - non reproductibles. Pourtant en discutant avec beaucoup de "speakers" depuis quelques années, j'ai retrouvé très souvent des éléments dans leur histoire : des personnes qui pensaient qu'elles n'avaient rien à raconter, d'autres qui les ont accueillies, des choses qu'on ne se croyait pas capable de faire et pourtant... des orateurs dont j'adore le travail qui sont pleins de doutes et de trac et n'acceptent qu'à moitié l'idée que ce qu'ils et elles proposent est important pour d'autres.

J'ai essayé de trouver des formats d'ateliers qui aideraient des personnes intéressées à se mettre à parler en public. J'ai donné quelques coups de pouce, quelques conseils, j'ai essayé retransmettre ceux qui m'avaient aidé. Mes ces conseils ne touchent que celles ou ceux qui en font la demande, c'est à dire qui ont déjà fait une bonne partie du chemin, et qui savent que je suis disponible pour les y aider, et pour qui je pourrais être la bonne personne.

J'ai donc essayé, en attendant que les [unconferences](https://en.wikipedia.org/wiki/Unconference) se généralisent, d'autres choses pour atténuer la barrière imaginaire et bien présente entre les *orateurs* et les *participants*.

J'essaie d'en présenter quelques unes ici.