# Notes: demarrage d'un projet rust sur STM32F103 (BluePill)

Les tutoriaux et sessions de live coding donnent souvent l'impression que démarrer un projet avec une technologie ou un environnement qu'on ne connait pas est simple. Et pourtant, tout le monde rame. Des fois pendant des jours.

Si vous voulez voir comment j'ai mis un week-end entier à arriver à faire clignoter une LED en Rust la première fois, et ceci malgré le fais que j'en avait fait clignoter des dizaines en assembleur, en C ou en LUA, sur des Pic, des Atmels, des Cortex, des ESP et même sur un 68000, mes notes sont [ici](https://github.com/FabienTregan/blink_bluepill_rust).