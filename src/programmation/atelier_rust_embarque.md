# Atelier Introduction à Rust et à l'Embarqué

Cet atelier est complètement en cours de test et d'écriture. Il devrait être possible de commencer à le tester rapidement en petit groupe avec des personnes intéressées et prêtes à essuyer les plâtres.

## Format

Cet atelier durera probablement trois jours. Pendant cette durée, les personnes participant assembleront une petite machine dessinant sur des notes adhésives de 78x78mm dont je ne citerais pas la marque.

![](plans_RustIt.svg "Le plan actuelle de la machine")

Il se déroulera en petits groupes (quatre à cinq participants par animateur, un ou deux animateurs, soit entre trois et douze personnes).

Il contiendra trois éléments distincts :
- Une partie de présentation des bases nécessaires d'électronique et de Rust pour démarrer, proche d'une formation standard.
- Des points préparés à l'avance et donnés sur demande sur des sujets spécifiques.
- Une grande partie (au moins la moitié du temps) de programmation en pair ou en mob, dans laquelle les animateurs n'interviennent que pour débloquer, donner les bons pointeurs, ou proposer les points préparés.

## Contenu

- Présentation des microcontrôleurs, de notions d'électronique, et de la machine.
- La couche d'abstraction du matériel proposée par Rust
- Flasher un firmware et debugger un STM32F103[^1] avec un STLink V2, OpenOCD et GDB
- Notions de base de programmation en Rust (fonctions, traits, monomorphisation, références, borrow checker / lifetimes, pattern matching, await / asynch )
- Navigation dans les différentes documentations (Datasheet du microcontrôleur, guide du coeur Cortex, Rust Book, Embedded Rust Book, documentation de l'API des crates utilisés, code source de Rust, et peut être le Rustonomicon)

Le but est que chaque personne participant ait une machine dessinant des formes simples (comme un spirographe) à la fin de la seconde journée, et la possibilité et l'envie de continuer après l'atelier les prolongements imaginés et commencés pendant la dernière journée.

## Public visé

Il s'adresse à des personnes sachant déjà programmer, ayant si possible deja quelques années de pratique de Java ou du C, et voulant démarrer un premier projet Rust et/ou voulant acquérir des notions de programmation sur microcontrôleur, par exemple pour mieux travailler avec une équipe embarquer s'ils doivent s'occuper du backend d'un projet IoT.

